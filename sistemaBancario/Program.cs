﻿using System;

namespace sistemaBancario
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sistema Bancário");
            double saldo = 0;
            double valor = 0;
            bool acabar = false;


            while (!acabar)
            {
                Console.WriteLine("Escolha opção:");
                Console.WriteLine("1 - Depósito");
                Console.WriteLine("2 - Saque");
                Console.WriteLine("3 - Consulta saldo");
                Console.WriteLine("4 - Sair");
                int menu = int.Parse(Console.ReadLine());
                switch (menu)
                {
                    case 1:
                        {
                            Console.WriteLine("Informe o valor a ser depositado");
                            valor = int.Parse(Console.ReadLine());
                            saldo = saldo + valor;
                            Console.WriteLine("Saldo = " + saldo);
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Informe o valor a ser sacado");
                            valor = int.Parse(Console.ReadLine());
                            if(saldo < valor){
                                Console.WriteLine("Saldo Insuficiente");
                            }
                            else
                            saldo = saldo - valor;
                            Console.WriteLine("Saldo = " + saldo);
                            break;
                        }

                    case 3:
                        {
                            Console.WriteLine("Consulta Saldo");
                            Console.WriteLine("Saldo = " + saldo);
                            break;
                        }

                    case 4:
                        {
                            Console.WriteLine("Sair");
                            acabar = true;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Opção errada...");
                            break;
                        }
                }
                Console.ReadKey();
            }
        }
    }
}
